# prefix-compression-test

Demonstration of using a known prefix to reduce compression size for
WebRTC SDPs with different compression algorithms.

`test-files/` contains examples from the IETF documents
[RFC 4566](https://tools.ietf.org/html/rfc4566) and
[draft-ietf-rtcweb-sdp-12](https://tools.ietf.org/id/draft-ietf-rtcweb-sdp-12.html).

As [a workaround](https://stackoverflow.com/a/61060583)
to allow [`fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
to work, run a local server
```
python3 -m http.server 8000 --bind 127.0.0.1
```
and open [http://localhost:8000/test.html](http://localhost:8000/test.html).
